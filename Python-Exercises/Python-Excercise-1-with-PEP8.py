# Machine Excercise 1
userInput = 0
while userInput >= 0:
    userInput = int(input("Enter a positive integer: "))
    if userInput >= 0:
        print("Output:")
        for l in range(userInput):
            # print(l, end="")
            for s in range((userInput) - l):
                print("  ", end="")
            for a in range(l * 2 - 1):
                print("* ", end="")
            print()
        for l in range(userInput, - 1, -1):
            # print(l, end="")
            for s in range(userInput - l):
                print("  ", end="")
            for a in range(l * 2 - 1):
                print("* ", end="")
            print()
    else:
        print()
        print("Invalid Input: please enter a positive integer")
        ext = str(input("Do you want to exit? Y/N : "))
        if ext == "Y" or ext == "y":
            exit()
        elif ext == "N" or ext == "n":
            userInput = 0
